describe("fileUpload", function () {
  it("fileUpload", () => {
    cy.visit("https://the-internet.herokuapp.com/upload");
    cy.get('[id="file-upload"]').selectFile("cypress/fixtures/picture1.JPG");
    cy.get('[id="file-submit"]').click();
    cy.get('[id="uploaded-files"]').should(
      "have.text",
      "\n    picture1.JPG\n  "
    );
  });
  it("multiWindow", () => {
    cy.visit("https://the-internet.herokuapp.com/windows");
    cy.get('[href="/windows/new"]').invoke("removeAttr", "target").click();
    cy.get('[class="example"]');
  });
  it("sortableData", () => {
    cy.visit("https://the-internet.herokuapp.com/tables");
    cy.get("#table1 > tbody > :nth-child(1) > :nth-child(1)").should(
      "have.text",
      "Smith"
    );
    cy.get("#table1 > tbody > :nth-child(2) > :nth-child(1)").should(
      "have.text",
      "Bach"
    );
    cy.get("#table1 > tbody > :nth-child(3) > :nth-child(1)").should(
      "have.text",
      "Doe"
    );
    cy.get("#table1 > tbody > :nth-child(4) > :nth-child(1)").should(
      "have.text",
      "Conway"
    );
    cy.get("#table1 > thead > tr > :nth-child(1) > span").click();
    cy.get("#table1 > tbody > :nth-child(1) > :nth-child(1)").should(
      "have.text",
      "Bach"
    );
    cy.get("#table1 > tbody > :nth-child(2) > :nth-child(1)").should(
      "have.text",
      "Conway"
    );
    cy.get("#table1 > tbody > :nth-child(3) > :nth-child(1)").should(
      "have.text",
      "Doe"
    );
    cy.get("#table1 > tbody > :nth-child(4) > :nth-child(1)").should(
      "have.text",
      "Smith"
    );
    cy.get("#table1 > tbody > :nth-child(1) > :nth-child(2)").should(
      "have.text",
      "Frank"
    );
    cy.get("#table1 > tbody > :nth-child(2) > :nth-child(2)").should(
      "have.text",
      "Tim"
    );
    cy.get("#table1 > tbody > :nth-child(3) > :nth-child(2)").should(
      "have.text",
      "Jason"
    );
    cy.get("#table1 > tbody > :nth-child(4) > :nth-child(2)").should(
      "have.text",
      "John"
    );
    cy.get("#table1 > thead > tr > :nth-child(2) > span").click();
    cy.get("#table1 > tbody > :nth-child(1) > :nth-child(2)").should(
      "have.text",
      "Frank"
    );
    cy.get("#table1 > tbody > :nth-child(2) > :nth-child(2)").should(
      "have.text",
      "Jason"
    );
    cy.get("#table1 > tbody > :nth-child(3) > :nth-child(2)").should(
      "have.text",
      "John"
    );
    cy.get("#table1 > tbody > :nth-child(4) > :nth-child(2)").should(
      "have.text",
      "Tim"
    );
    cy.get('[class="last-name"]').eq(1).should("have.text", "Smith");
    cy.get('[class="last-name"]').eq(2).should("have.text", "Bach");
    cy.get('[class="last-name"]').eq(3).should("have.text", "Doe");
    cy.get('[class="last-name"]').eq(4).should("have.text", "Conway");
    cy.get('[class="last-name"]').eq(0).click();
    cy.get('[class="last-name"]').eq(1).should("have.text", "Bach");
    cy.get('[class="last-name"]').eq(2).should("have.text", "Conway");
    cy.get('[class="last-name"]').eq(3).should("have.text", "Doe");
    cy.get('[class="last-name"]').eq(4).should("have.text", "Smith");
    cy.get('[class="first-name"]').eq(1).should("have.text", "Frank");
    cy.get('[class="first-name"]').eq(2).should("have.text", "Tim");
    cy.get('[class="first-name"]').eq(3).should("have.text", "Jason");
    cy.get('[class="first-name"]').eq(4).should("have.text", "John");
    cy.get('[class="first-name"]').eq(0).click();
    cy.get('[class="first-name"]').eq(1).should("have.text", "Frank");
    cy.get('[class="first-name"]').eq(2).should("have.text", "Jason");
    cy.get('[class="first-name"]').eq(3).should("have.text", "John");
    cy.get('[class="first-name"]').eq(4).should("have.text", "Tim");
  });
});
